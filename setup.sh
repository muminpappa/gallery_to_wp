#!/bin/bash
python3 -m venv --system-site-packages venv
source venv/bin/activate
python -m pip install bs4 requests
deactivate
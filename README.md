# Old Photo Gallery to Wordpress

## Goal

I have a whole list of photo pages that
I would like to import into my Wordpress
site, at least with a thumbnail and a link.

## Preparation

Install the plugin https://github.com/WordPress/application-passwords 
in your wordpress instance.

Create a new application password for your user
(see plugin documentation).

Store the application password in file `apikey`
and set environment variable:

    export APPLICATIONPASSWORD=$(cat apikey )

Check that it works (modify title of post with ID 1):

    curl --user "axel:${APPLICATIONPASSWORD}" -X POST -d "title=New Axel Title" https://playground.mumintroll.org/wp-json/wp/v2/posts/1

Install jq, e.g.

    brew install jq

Get the endpoints so we have a place to look up parameters easily
(just open the file with a browser):

    curl --user "axel:${APPLICATIONPASSWORD}" -X GET https://playground.mumintroll.org/wp-json/ | jq > endpoints.json

Go to Settings > Permalinks and change the setting to "Post name" to get it to work.
https://stackoverflow.com/questions/23842235/wordpress-jsonapi-wp-json-was-not-found-on-this-server

## Scrape existing HTML pages

## Create new post using Python

https://wordpress.stackexchange.com/questions/328249/how-to-use-python-to-create-a-post-in-wordpress 

https://developer.wordpress.org/rest-api/reference/posts/ 

## Uplad images to Wordpress

Helpful: https://stackoverflow.com/questions/43915184/how-to-upload-images-using-wordpress-rest-api-in-python 

Using the `files` argument in `post()`:
https://requests.readthedocs.io/en/master/user/quickstart/#post-a-multipart-encoded-file 

File extension: https://stackoverflow.com/questions/678236/how-to-get-the-filename-without-the-extension-from-a-path-in-python 

Unique filename: `uuid.uuid4().hex`


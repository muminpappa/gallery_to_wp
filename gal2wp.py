from bs4 import BeautifulSoup
import re
import requests
import json
import os
from datetime import datetime
import uuid
import wget
import urllib

silent = True

src_url = "https://mumintroll.org/galerie/bilder/"
index_file = src_url+"index.html"

WP_URL = 'https://familjen.mumintroll.org'

def parse_index(index_file:str):
    r=requests.get(index_file)
    soup = BeautifulSoup(r.content, 'html.parser',from_encoding=r.apparent_encoding)

    silent or print(soup)
    
    # a dictionary with all the linked pages
    page_list = []

    re_date = re.compile(r'^(\d{4})-(\d{2})-([\dX]{2})')
    for link in soup.find_all('tr'):        
        l = link.contents
        try:
            lp = l[1].find('a').get('href')
        except AttributeError as err:
            silent or print('Empty row, '+str(l[1])+str(err))
            continue
    
        event_date = re.match(re_date, lp)
        if(event_date):
            page = {}
            page['year']=event_date[1]
            page['month']=event_date[2]
            page['day']=re.sub('X','0',event_date[3])
            page['thumbnail']=l[1].find('img').get('src')
            page['title']=l[3].find_all('p')[0].get_text(' ',strip=True).replace('\n',' ').replace('\t','')
            page['content']=l[3].get_text(' ',strip=True).replace('\n',' ').replace('\t','')
            page['link']=lp
            page_list.append(page)
    return page_list

def create_wp_post(wordpressUrl:str, src_page:dict, featured_media:int):
    APPLICATIONPASSWORD=os.environ['APPLICATIONPASSWORD']
    USER=os.environ['USER']
    content='<a href="'+src_url+src_page['link']+'">'+src_page['content']+'</a>'
    day = int(src_page['day'])
    params = {
        'date': datetime(year=int(src_page['year']),month=int(src_page['month']),day=day>0 and day or 1,hour=10).isoformat(),
        'status': 'publish',
        'title': src_page['title'],
        'author': 1,
        'content': content,
        'featured_media': featured_media
    }
    r = requests.post(url=wordpressUrl+"/wp-json/wp/v2/posts",
    json=params,auth=(USER, APPLICATIONPASSWORD))
    try:
        silent or print(json.dumps(r.json(),sort_keys=True, indent=4))
        silent or print(r.status_code)
        silent or print(r.headers)
        postId = r.json()['id']
        print('ID of created post: '+str(postId))
    except ValueError as err:
        print('Failed to create post: '+str(err))
        postId = None
    return postId

def upload_image(wordpressUrl:str, filepath:str):
    APPLICATIONPASSWORD=os.environ['APPLICATIONPASSWORD']
    USER=os.environ['USER']
    # make a unique filename for the wordpress media library
    filename = uuid.uuid4().hex + os.path.splitext(filepath)[1]
    files = {'file': (filename, open(filepath,'rb'))}
    r = requests.post(url=wordpressUrl+"/wp-json/wp/v2/media",
    files=files,auth=(USER, APPLICATIONPASSWORD))
    try:
        silent or print(json.dumps(r.json(),sort_keys=True, indent=4))
        silent or print(r.status_code)
        silent or print(r.headers)
        imageId = r.json()['id']
        print('ID of uploaded image: '+str(imageId))
    except ValueError as err:
        print('Image upload failed '+str(err))
        imageId = None
    return imageId

page_list = parse_index(index_file)   
for p in page_list[1:]:
    featured_media_url = src_url + p['thumbnail'].replace('thumbnail','picture').replace('thumb','picture')
    silent or print(featured_media_url)
    try:
        featured_media_fn = wget.download(featured_media_url)
    except urllib.error.HTTPError as err:
        silent or print('Featured media not found in '+featured_media_url)
        continue
    imageId = upload_image(wordpressUrl=WP_URL,filepath=featured_media_fn)
    os.remove(featured_media_fn)
    postId = create_wp_post(
        wordpressUrl=WP_URL,
        src_page=p,
        featured_media=imageId)
